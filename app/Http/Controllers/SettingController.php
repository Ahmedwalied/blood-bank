<?php
namespace App\Http\Controllers;
use App\Models\Setting;
use Illuminate\Http\Request;
class SettingController extends Controller
{
    public function index()
    {
//        return view('settings.edit');
        return view('setting')->with('settings', Setting::first());
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required',
            'email' => 'required',
            'whatsup_url' => 'required',
            'instgram_url' => 'required',
            'about_us' => 'required|nullable',
            'youtube_url' => 'required',
            'twitter_url' => 'required',
            'facebook_url' => 'required',
            'link_url' => 'nullable',
            'image' => 'nullable|image',
        ]);
        $setting = Setting::first();
        if ( $request->hasFile('image')  ) {
            if (file_exists($setting->logo)) {
                unlink($setting->logo);
            }
            $logo = $request->logo;
            $logo_new_name = time() . $logo->getClientOriginalName();
            $logo->move('uploads/setting', $logo_new_name);
            $setting->logo = 'uploads/setting/'.$logo_new_name;
        }
//        if ( $request->hasFile('logo')  ) {
//            $featured = $request->logo;
//            $featured_new_name = time().$featured->getClientOriginalName();
//            $featured->move('uploads/posts/',$featured_new_name);
//
//            $setting->logo = 'uploads/posts/'.$featured_new_name;
//
//        }
        $setting->phone = $request->phone;
        $setting->email = $request->email;
        $setting->link_url = $request->link_url;
        $setting->whatsup_url = $request->whatsup_url;
        $setting->about_us = $request->about_us;
        $setting->instgram_url = $request->instgram_url;
        $setting->youtube_url = $request->youtube_url;
        $setting->twitter_url = $request->twitter_url;
        $setting->facebook_url = $request->facebook_url;
        $setting->image = $request->image;
        $setting->save();
        flash()->success("تم التعديل بنجاح");
        return redirect()->back();
    }
}
